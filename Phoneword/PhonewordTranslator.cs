﻿using System.Text;

namespace Core
{
    public static class PhonewordTranslator
    {

        //  Converts text to phone number
        public static string ToNumber(string raw)
        {
            if (string.IsNullOrWhiteSpace(raw)) return null;

            raw = raw.ToUpperInvariant();

            var newNumber = new StringBuilder();
            foreach (var c in raw)
            {
                // if text is number, add to phonenumber
                if (" -0123456789".Contains(c))
                    newNumber.Append(c);
                else
                // convert to num based on letters on telephone keypad and add to phoneNumber
                {
                    var result = TranslateToNumber(c);
                    if (result != null)
                        newNumber.Append(result);
                    // Bad character?
                    else
                        return null;
                }
            }
            return newNumber.ToString();
        }

        // Extension method that checks if a char is contained within a string
        static bool Contains(this string keyString, char c)
        {
            return keyString.IndexOf(c) >= 0;
        }

        static readonly string[] digits =
        {
            "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ"
        };

        static int? TranslateToNumber(char c)
        {
            for(int i = 0; i< digits.Length; i++)
            {
                if (digits[i].Contains(c))
                    return 2 + i;
            }
            return null;
        }
    }
}
